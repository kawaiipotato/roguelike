﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public int playerCurrentHealth = 50;
    public static GameController Instance;
    public bool isPlayerTurn;
    public AudioClip gameOverSound;
    public bool areEnemiesMoving;
    public GameObject normalPlayer;
    public GameObject knightPlayer;
    public int characterSelected;
    public GameObject regularPlayerButton;
    public GameObject knightPlayerButton;
    public Text healthText;

    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject titleImage;
    private Text titleText;
    private GameObject levelImage;
    private Text levelText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;
    private bool elegibleForClick;
    private readonly object pointerPress;

    void Awake()
    {

        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;

        }
        Instance = this;
        DontDestroyOnLoad(gameObject);//for us not to destory game object on each level change
        DontDestroyOnLoad(healthText);
        boardController = GetComponent<BoardController>();
        knightPlayer = GameObject.FindGameObjectWithTag("Knight");
        normalPlayer = GameObject.FindGameObjectWithTag("Player");
        enemies = new List<Enemy>();

    }

    void Start() {
        TitleImage();
    }

    private void TitleImage()
    {
        settingUpGame = true;
        titleImage = GameObject.Find("Title Image");
        titleImage.SetActive(true);
        knightPlayerButton = GameObject.Find("Knight Button");
        regularPlayerButton = GameObject.Find("Regular Button");

    }

    //function to get the choose player thing
   public void Knight() {
        characterSelected = 0;
        DisableTitleImage();
    }
        public void Human()
        {
            characterSelected = 1;
            DisableTitleImage();
        }

    private void DisableTitleImage()
    {
        titleImage.SetActive(false);
        isPlayerTurn = true;
        areEnemiesMoving = false;
        settingUpGame = false;
        InitializeGame();
    }



    private void InitializeGame()
    {
        if (currentLevel > 1) {
            titleImage = GameObject.Find("Title Image");
            titleImage.SetActive(false);
        }
        healthText = GameObject.Find("Health Text").GetComponent<Text>();
        settingUpGame = true;
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetUpLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);
        
    }

    private void DisableLevelImage() {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }

    void Update()
    {

        if (isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }
        StartCoroutine(MoveEnemies());

    }

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);//we wantplayer to fiinish movement
        

        foreach (Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }


        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void addEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void GameOver()
    {
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        levelText.text = "You starved after " + currentLevel + " days...";
        levelImage.SetActive(true);
        enabled = false;
    }
}
