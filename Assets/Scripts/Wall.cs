﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    public int hitPoint = 2;
    public Sprite damagedWallSprite;
    private SpriteRenderer spriteRenderer;
	
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}

    public void DamageWall(int damageRecieved)
    {
        hitPoint -= damageRecieved;
        spriteRenderer.sprite = damagedWallSprite;
        //Debug.Log(hitPoint);

        if (hitPoint <= 0)
        {

            gameObject.SetActive(false);
        }
    }
	
	
	
}
